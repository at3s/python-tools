# Python Tools - Tools by Python with love 💞

### Md5 File Checksum

cd md5-file-checksum && python3 md5FileChecksum.py file_path md5_string

### Watch File ( track python or js file change then run this file )

cd watch-file && python3 watch.py js_or_python_file_path
