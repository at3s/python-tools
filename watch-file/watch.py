import sys
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import subprocess
import os

ext_supported = {
    "py": "python3",
    "js": "node"
}

file_path = sys.argv[1] if len(sys.argv) > 1 else ''
ext = file_path.split('.')[-1]

if file_path == '':
    print('Missing file path :)')
    exit()

if not os.path.isfile(file_path):
    print('File not found nhe :)')
    exit()

if not '.py' == '.'+ext and not '.js' == '.'+ext:
    print("Don't support file type :)")
    exit()


class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if(event.src_path == file_path):
            output = subprocess.run(
                [ext_supported[ext], file_path], stdout=subprocess.PIPE, text=True)
            os.system('clear')
            print(output.stdout)


event_handler = MyHandler()
observer = Observer()
observer.schedule(event_handler, path=file_path, recursive=False)
observer.start()
print('Watching ' + file_path)

while True:
    try:
        pass
    except KeyboardInterrupt:
        observer.stop()
        exit()
