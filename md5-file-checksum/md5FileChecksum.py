#!/usr/bin/python
import hashlib
import sys

argv = sys.argv

def md5():
    try:
        filePath = argv[1]
        md5String = argv[2]
        hash_md5 = hashlib.md5()
        with open(filePath, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        if hash_md5.hexdigest() == md5String:
            print('ok')
        else:
            print('error')
    except FileNotFoundError:
        print('File Not Found')

if len(argv) < 3:
    print('Missing argument, require two, path of file then md5 string')
else:
    md5()